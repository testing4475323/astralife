package astralife.employee.tes.model;

import java.time.LocalDate;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import jakarta.persistence.Entity;
import jakarta.persistence.EntityListeners;
import jakarta.persistence.FetchType;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@EntityListeners(AuditingEntityListener.class)
@Entity(name = "deptManager")
public class deptManagerModel extends mainModel{
    @JoinColumn(name = "employees", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private employeesModel employees;
    @JoinColumn(name = "departments", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private departmentsModel departments;
    private LocalDate fromDate;
    private LocalDate toDate;
}
