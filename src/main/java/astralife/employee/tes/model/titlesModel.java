package astralife.employee.tes.model;

import java.time.LocalDate;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EntityListeners;
import jakarta.persistence.FetchType;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@EntityListeners(AuditingEntityListener.class)
@Entity(name = "titles")
public class titlesModel extends mainModel{
    @JoinColumn(name = "employees", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private employeesModel employees;
    @Column(unique = true, length = 50, nullable = false)
    private String title;
    @Column(unique = true, nullable = false)
    private LocalDate fromDate;
    private LocalDate toDate;
}
