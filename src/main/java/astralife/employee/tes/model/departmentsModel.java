package astralife.employee.tes.model;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EntityListeners;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@EntityListeners(AuditingEntityListener.class)
@Entity(name = "departments")
public class departmentsModel extends mainModel{
    @Column(unique = true, length = 4, nullable = false)
    private String deptNo;
    @Column(length = 40)
    private String deptName;
}
