package astralife.employee.tes.model;

import java.time.LocalDateTime;

import org.hibernate.annotations.GenericGenerator;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import jakarta.persistence.EntityListeners;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.MappedSuperclass;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@MappedSuperclass
@EntityListeners(AuditingEntityListener.class)
public class mainModel {
	@Id 
    @GenericGenerator(name = "customId", strategy = "astralife.employee.tes.util.idGeneratorUtil")
    @GeneratedValue(generator = "customId")  
    private String id;
    private LocalDateTime createdAt;
    private LocalDateTime updatedAt;
}
