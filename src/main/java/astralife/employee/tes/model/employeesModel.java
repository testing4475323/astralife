package astralife.employee.tes.model;

import java.time.LocalDate;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import astralife.employee.tes.enumType.genderEnumType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EntityListeners;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@EntityListeners(AuditingEntityListener.class)
@Entity(name = "employees")
public class employeesModel extends mainModel {
    @Column(unique = true, length = 11, nullable = false)
    private int empNo;
    private LocalDate birthDate;
    @Column(length = 14)
    private String firstName;
    @Column(length = 16)
    private String lastName;
    @Enumerated(EnumType.STRING)
    private genderEnumType gender;
    private LocalDate hireDate;
}
