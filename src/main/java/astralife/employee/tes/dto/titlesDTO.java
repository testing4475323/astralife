package astralife.employee.tes.dto;

import java.time.LocalDate;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class titlesDTO {
    @Size(max = 50, message = "Title is max 50 character")
    @NotBlank(message = "Title is required")
    private int title;
    @NotBlank(message = "From Date is required")
    private LocalDate fromDate;
    private LocalDate toDate;
}
