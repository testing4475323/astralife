package astralife.employee.tes.dto;

import java.time.LocalDate;
import java.util.List;

import astralife.employee.tes.enumType.genderEnumType;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class employeesDTO extends mainDTO{
    @Size(max = 11, message = "Employee Number is max 11 character")
    @NotBlank(message = "Employee Number is required")
    private String empNo;
    private LocalDate birthDate;
    @Size(max = 14, message = "First Name is max 14 character")
    private String firstName;
    @Size(max = 16, message = "Last Name is max 16 character")
    private String lastName;
    private genderEnumType gender;
    private LocalDate hireDate;
    private List<deptDTO> deptEmp;
    private List<deptDTO> deptManager;
    private List<salariesDTO> salaries;
    private List<titlesDTO> titles;
}
