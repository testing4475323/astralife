package astralife.employee.tes.dto;

import java.time.LocalDate;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class deptDTO {
    private LocalDate fromDate;
    private LocalDate toDate;
    @NotBlank(message = "Departments Id is required")
    private String departmentsId;
    private departmentsSUBDTO departments;

    @Getter
    @Setter
    private static class departmentsSUBDTO{
        @NotBlank(message = "Departments Number is required")
        @Size(max = 4, message = "Departments Number is max 4 character")
        private String deptNo;
        @Size(max = 40, message = "Departments Name is max 40 character")
        private String deptName;
    }
}
