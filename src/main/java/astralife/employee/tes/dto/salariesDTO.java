package astralife.employee.tes.dto;

import java.time.LocalDate;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class salariesDTO {
    @Size(max = 11, message = "Salary is max 11 character")
    @NotBlank(message = "Salary is required")
    private int salary;
    @NotBlank(message = "From Date is required")
    private LocalDate fromDate;
    private LocalDate toDate;
}
