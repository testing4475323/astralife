package astralife.employee.tes.dto;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class departmentsDTO extends mainDTO{
    @NotBlank(message = "Departments Number is required")
    @Size(max = 4, message = "Departments Number is max 4 character")
    private String deptNo;
    @Size(max = 40, message = "Departments Name is max 40 character")
    private String deptName;
}
