package astralife.employee.tes.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import astralife.employee.tes.model.employeesModel;

public interface employeesRepository extends JpaRepository<employeesModel, String> {    
}
