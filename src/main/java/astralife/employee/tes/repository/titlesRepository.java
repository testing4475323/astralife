package astralife.employee.tes.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import astralife.employee.tes.model.employeesModel;
import astralife.employee.tes.model.titlesModel;

public interface titlesRepository extends JpaRepository<titlesModel, String>{
    List<titlesModel> findByEmployees(employeesModel employees);
}
