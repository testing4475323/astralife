package astralife.employee.tes.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import astralife.employee.tes.model.employeesModel;
import astralife.employee.tes.model.salariesModel;

public interface salariesRepository extends JpaRepository<salariesModel, String>{
    List<salariesModel> findByEmployees(employeesModel employees);
}
