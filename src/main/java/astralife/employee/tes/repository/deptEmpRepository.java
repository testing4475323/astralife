package astralife.employee.tes.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import astralife.employee.tes.model.departmentsModel;
import astralife.employee.tes.model.deptEmpModel;
import astralife.employee.tes.model.employeesModel;

public interface deptEmpRepository extends JpaRepository<deptEmpModel, String> {
    List<deptEmpModel> findByEmployees(employeesModel employees);
    List<deptEmpModel> findByDepartments(departmentsModel departments);
}
