package astralife.employee.tes.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import astralife.employee.tes.model.departmentsModel;
import astralife.employee.tes.model.deptManagerModel;
import astralife.employee.tes.model.employeesModel;

public interface deptManagerRepository extends JpaRepository<deptManagerModel, String>{
    List<deptManagerModel> findByEmployees(employeesModel employees);
    List<deptManagerModel> findByDepartments(departmentsModel departments);
}
