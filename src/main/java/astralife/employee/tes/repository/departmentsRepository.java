package astralife.employee.tes.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import astralife.employee.tes.model.departmentsModel;

public interface departmentsRepository extends JpaRepository<departmentsModel, String>{
}
