package astralife.employee.tes.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import astralife.employee.tes.dto.departmentsDTO;
import astralife.employee.tes.service.departmentsService;
import io.swagger.v3.oas.annotations.Operation;
import jakarta.validation.Valid;

@RestController
public class departmentsController {
    @Autowired
    private departmentsService ds;
    @PostMapping("departments")
    @Operation(summary = "Create Departments")
    public Object createDepartments(@RequestBody @Valid departmentsDTO dd){
        return ds.createDepartments(dd);
    }
    @PutMapping("departments/{id}")
    @Operation(summary = "Update Departments")
    public Object updateDepartments(@PathVariable String id, @RequestBody @Valid departmentsDTO dd){
        return ds.updateDepartments(id, dd);
    }
    @DeleteMapping("departments/{id}")
    @Operation(summary = "Delete Departments")
    public Object deleteDepartments(@PathVariable String id){
        return ds.deleteDepartments(id);
    }
    @GetMapping("departments")
    @Operation(summary = "Get All Departments")
    public Object getAllDepartments(){
        return ds.getAllDepartments();
    }
}
