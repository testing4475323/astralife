package astralife.employee.tes.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import astralife.employee.tes.dto.employeesDTO;
import astralife.employee.tes.service.employeesService;
import io.swagger.v3.oas.annotations.Operation;
import jakarta.validation.Valid;

@RestController
public class employeesController {
    @Autowired
    private employeesService es;
    @PostMapping("employees")
    @Operation(summary = "Create Employees")
    public Object createEmployees(@RequestBody @Valid employeesDTO ed){
        return es.createEmployees(ed);
    }
    @PutMapping("employees/{id}")
    @Operation(summary = "Update Employees")
    public Object updateEmployees(@PathVariable String id, @RequestBody @Valid employeesDTO ed){
        return es.updateEmployees(id, ed);
    }
    @DeleteMapping("employees/{id}")
    @Operation(summary = "Delete Employees")
    public Object deleteEmployees(@PathVariable String id){
        return es.deleteEmployees(id);
    }
    @GetMapping("employees")
    @Operation(summary = "Get All Employees")
    public Object getAllEmployees(){
        return es.getAllEmployees();
    }
}
