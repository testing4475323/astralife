package astralife.employee.tes;

import java.util.TimeZone;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TesApplication {

	public static void main(String[] args) {
		SpringApplication.run(TesApplication.class, args);
		header();
	}
    public static void header(){
		TimeZone.setDefault(TimeZone.getTimeZone("Asia/Bangkok"));
		System.out.println("\n");
		System.out.println("\u001B[33m\t employees-be R0623 X BETA");
		System.out.println("\u001B[34m\t\t Version employees-be R0623 BETA");
		System.out.println("\u001B[35m\t\t\t Powered by Astralife");
		System.out.println("\u001B[0m\n");
    }

}
