package astralife.employee.tes.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.servers.Server;

@Configuration
@OpenAPIDefinition
public class openApiConfig {
    @Value("${astralife.version.name}")
	private String name;
    @Value("${astralife.version.code}")
	private String code;
    @Bean
    public OpenAPI costumeOpenApi(){
        return new OpenAPI().info(new Info().title("Users Document Management").version(code).description("employees-be R0623 X "+name+" (employees-be R0623 "+code+")")).addServersItem(new Server().url("/").description("Default Server URL"));
    }
}
