package astralife.employee.tes.config;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

import astralife.employee.tes.util.requestFilterUtil;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Configuration
public class globalConfig {
    @Autowired
    private requestFilterUtil rfu;
    @Bean
    public SecurityFilterChain filterChain(HttpSecurity http){
        try {
            http.csrf(c -> 
                c.disable()).authorizeHttpRequests(a->
                    a.requestMatchers(
                        "/**"
                    ).permitAll().anyRequest().authenticated()
                ).sessionManagement(s->s.sessionCreationPolicy(SessionCreationPolicy.STATELESS)
            );
            http.addFilterBefore(rfu, UsernamePasswordAuthenticationFilter.class);
            http.headers(h->
                h.frameOptions(f->
                    f.sameOrigin()
                )
            );
            return http.build();  
        } catch (Exception e) {
            log.error("securityConfig.filterChain => "+ e.getMessage());
            return null;           
        }
    }
    @Bean
	public WebMvcConfigurer corsConfigurer() {
		return new WebMvcConfigurer() {
			@Override
			public void addCorsMappings(CorsRegistry registry) {
				registry.addMapping("/**").allowedOrigins("*").allowedMethods("*");
			}
		};
	}
    @Bean
    public ModelMapper modelMapper() {
        return new ModelMapper();
    }
    @Bean
    public ObjectMapper objectMapper() {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.registerModule(new JavaTimeModule());
        return objectMapper;
    }
}
