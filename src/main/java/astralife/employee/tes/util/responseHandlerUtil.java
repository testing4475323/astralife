package astralife.employee.tes.util;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import astralife.employee.tes.dto.responseDTO;
import astralife.employee.tes.enumType.responseCodeEnumType;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class responseHandlerUtil {
    public static ResponseEntity<Object> generateResponse(responseCodeEnumType rcet, HttpStatus status, Object value){
        responseDTO rd = setDefault(rcet, status);
        rd.setValue(value);
        return new ResponseEntity<Object>(rd, status);
    }
    public static ResponseEntity<Object> generateResponse(responseCodeEnumType rcet, HttpStatus status){
        responseDTO rd = setDefault(rcet, status);
        rd.setTitle(status.getReasonPhrase());
        return new ResponseEntity<Object>(rd, status);
    }
    public static ResponseEntity<Object> generateResponse(responseDTO rd, HttpStatus status){
        rd.setTitle(status.getReasonPhrase());
        log.info(rd.getErrorCode()+" "+rd.getTitle()+", "+rd.getMessage());;
        return new ResponseEntity<Object>(rd, status);
    }
    public static ResponseEntity<Object> generateResponse(Object value){
        HttpStatus status = HttpStatus.OK;
        responseDTO rd = setDefault(responseCodeEnumType.e00, status);
        rd.setValue(value);
        return new ResponseEntity<Object>(rd, status);
    }
    public static ResponseEntity<Object> generateResponse(){
        HttpStatus status = HttpStatus.OK;
        responseDTO rd = setDefault(responseCodeEnumType.e00, status);
        return new ResponseEntity<Object>(rd, status);
    }
    private static responseDTO setDefault(responseCodeEnumType rcet, HttpStatus status){
        responseDTO rd = new responseDTO();
        rd.setErrorCode(rcet.toString().replace("e", ""));
        rd.setMessage(rcet.getValue());
        rd.setTitle(status.getReasonPhrase());
        log.info(rd.getErrorCode()+" "+rd.getTitle()+", "+rd.getMessage());
        return rd;
    }
}
