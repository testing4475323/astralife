package astralife.employee.tes.util;

import java.io.Serializable;
import java.util.UUID;

import org.hibernate.engine.spi.SharedSessionContractImplementor;
import org.hibernate.id.IdentifierGenerator;
import org.springframework.stereotype.Component;

@Component
public class idGeneratorUtil implements IdentifierGenerator{
    @Override
    public Serializable generate(SharedSessionContractImplementor session, Object object) {
        String uuid = UUID.randomUUID().toString();
        String[] parts = uuid.split("-");
        StringBuilder generatedId  = new StringBuilder();
        for (String part : parts) {
            generatedId.append(part);
            if(part.equals(parts[parts.length - 1])){
                int random = (int) (Math.random() * 10);
                generatedId.append(random);
            }
        }
        return generatedId.toString();
    }
}
