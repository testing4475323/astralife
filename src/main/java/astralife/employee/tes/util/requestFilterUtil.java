package astralife.employee.tes.util;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import com.fasterxml.jackson.databind.ObjectMapper;

import astralife.employee.tes.enumType.responseCodeEnumType;
import jakarta.servlet.FilterChain;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class requestFilterUtil extends OncePerRequestFilter{
    @Autowired
    private ObjectMapper mapper;
    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) {
        try {
            log.info("Path: "+request.getRequestURI());
            filterChain.doFilter(request, response);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("requestFilterUtil.doFilterInternal -> "+ e.getMessage());
            customResponse(response, responseCodeEnumType.e01);
        }
    }
    private void customResponse(HttpServletResponse response, responseCodeEnumType rce){
        try {
            ResponseEntity<Object> reo = responseHandlerUtil.generateResponse(rce, HttpStatus.FORBIDDEN);
            response.setStatus(reo.getStatusCode().value());
            response.setContentType("application/json");
            response.setCharacterEncoding("UTF-8");
            response.setHeader("Access-Control-Allow-Origin", "*");
            response.getWriter().println(mapper.writerWithDefaultPrettyPrinter().writeValueAsString(reo.getBody()));
        } catch (Exception e) {
            log.error("mainService.customResponse -> "+e.getMessage());
        }
    }
}
