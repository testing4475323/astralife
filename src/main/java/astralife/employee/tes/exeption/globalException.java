package astralife.employee.tes.exeption;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.zalando.problem.StatusType;

import astralife.employee.tes.dto.responseDTO;
import astralife.employee.tes.enumType.responseCodeEnumType;
import astralife.employee.tes.util.responseHandlerUtil;

@ControllerAdvice
public class globalException {
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<Object> handleException(MethodArgumentNotValidException manve) {
        List<String> errors = manve.getBindingResult().getFieldErrors().stream().map(DefaultMessageSourceResolvable::getDefaultMessage).collect(Collectors.toList());
        return responseHandlerUtil.generateResponse(responseCodeEnumType.e02, HttpStatus.BAD_REQUEST, errors);
    }
    @ExceptionHandler(DataIntegrityViolationException.class)
    public ResponseEntity<Object> handleException(DataIntegrityViolationException dive) {
        return responseHandlerUtil.generateResponse(responseCodeEnumType.e07, HttpStatus.CONFLICT, dive.getMessage());
    }

    @ExceptionHandler(badRequestException.class)
    public ResponseEntity<Object> handleException(badRequestException bre) {
        return setDefault(bre.getTitle(), bre.getDetail(), bre.getStatus());
    }
    @ExceptionHandler(internalServerErrorException.class)
    public ResponseEntity<Object> handleException(internalServerErrorException isee) {
        return setDefault(isee.getTitle(), isee.getDetail(), isee.getStatus());
    }
    private ResponseEntity<Object> setDefault(String statusCode, String message, StatusType st){
        responseDTO rd = new responseDTO();
        rd.setErrorCode(statusCode.replace("e", ""));
        rd.setMessage(message);
        return responseHandlerUtil.generateResponse(rd, HttpStatus.valueOf(st.getStatusCode()));
    }
}
