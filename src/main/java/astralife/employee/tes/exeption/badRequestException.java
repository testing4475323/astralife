package astralife.employee.tes.exeption;

import java.net.URI;

import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.zalando.problem.AbstractThrowableProblem;
import org.zalando.problem.Status;

import astralife.employee.tes.enumType.responseCodeEnumType;

public class badRequestException extends AbstractThrowableProblem{
    private static final URI uriType =  ServletUriComponentsBuilder.fromCurrentRequest().build().toUri();
    public badRequestException(responseCodeEnumType rcet){
        super(uriType, rcet.toString(), Status.BAD_REQUEST, rcet.getValue());
    }
}
