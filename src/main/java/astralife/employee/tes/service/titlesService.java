package astralife.employee.tes.service;

import java.time.LocalDateTime;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import astralife.employee.tes.enumType.responseCodeEnumType;
import astralife.employee.tes.exeption.internalServerErrorException;
import astralife.employee.tes.model.titlesModel;
import astralife.employee.tes.repository.titlesRepository;
import jakarta.transaction.Transactional;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
@Transactional
public class titlesService {
    @Autowired
    private titlesRepository ts;
    public titlesModel create(titlesModel tm, LocalDateTime createdAt){
        try {
            tm.setCreatedAt(createdAt);
            ts.save(tm);
            return tm;
        } catch (Exception e) {
            log.error("titlesService.create -> "+e.getMessage());
            throw new internalServerErrorException(responseCodeEnumType.e04);
        }
    }
}
