package astralife.employee.tes.service;

import java.time.LocalDateTime;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import astralife.employee.tes.enumType.responseCodeEnumType;
import astralife.employee.tes.exeption.internalServerErrorException;
import astralife.employee.tes.model.deptEmpModel;
import astralife.employee.tes.repository.deptEmpRepository;
import jakarta.transaction.Transactional;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
@Transactional
public class deptEmpService {
    @Autowired
    private deptEmpRepository der;
    public deptEmpModel create(deptEmpModel dem, LocalDateTime createdAt){
        try {
            dem.setCreatedAt(createdAt);
            der.save(dem);
            return dem;
        } catch (Exception e) {
            log.error("deptEmpService.create -> "+e.getMessage());
            throw new internalServerErrorException(responseCodeEnumType.e04);
        }
    }
}
