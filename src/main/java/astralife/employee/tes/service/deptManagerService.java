package astralife.employee.tes.service;

import java.time.LocalDateTime;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import astralife.employee.tes.enumType.responseCodeEnumType;
import astralife.employee.tes.exeption.internalServerErrorException;
import astralife.employee.tes.model.deptManagerModel;
import astralife.employee.tes.repository.deptManagerRepository;
import jakarta.transaction.Transactional;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
@Transactional
public class deptManagerService {
    @Autowired
    private deptManagerRepository dmr;
    public deptManagerModel create(deptManagerModel dmm, LocalDateTime createdAt){
        try {
            dmm.setCreatedAt(createdAt);
            dmr.save(dmm);
            return dmm;
        } catch (Exception e) {
            log.error("deptManagerService.create -> "+e.getMessage());
            throw new internalServerErrorException(responseCodeEnumType.e04);
        }
    }
}
