package astralife.employee.tes.service;

import java.time.LocalDateTime;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import astralife.employee.tes.dto.deptDTO;
import astralife.employee.tes.dto.employeesDTO;
import astralife.employee.tes.dto.salariesDTO;
import astralife.employee.tes.dto.titlesDTO;
import astralife.employee.tes.enumType.responseCodeEnumType;
import astralife.employee.tes.exeption.badRequestException;
import astralife.employee.tes.exeption.internalServerErrorException;
import astralife.employee.tes.model.deptEmpModel;
import astralife.employee.tes.model.deptManagerModel;
import astralife.employee.tes.model.employeesModel;
import astralife.employee.tes.model.salariesModel;
import astralife.employee.tes.model.titlesModel;
import astralife.employee.tes.repository.deptEmpRepository;
import astralife.employee.tes.repository.deptManagerRepository;
import astralife.employee.tes.repository.employeesRepository;
import astralife.employee.tes.repository.salariesRepository;
import astralife.employee.tes.repository.titlesRepository;
import astralife.employee.tes.util.responseHandlerUtil;
import jakarta.transaction.Transactional;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
@Transactional
public class employeesService {
    @Autowired
    private ModelMapper mm;
    @Autowired
    private employeesRepository er;
    @Autowired
    private deptEmpService des;
    @Autowired
    private deptManagerService dms;
    @Autowired
    private salariesService ss;
    @Autowired
    private titlesService ts;
    @Autowired
    private deptEmpRepository der;
    @Autowired
    private deptManagerRepository dmr;
    @Autowired
    private salariesRepository sr;
    @Autowired
    private titlesRepository tr;
    public Object createEmployees(employeesDTO ed){
        LocalDateTime ldt = LocalDateTime.now();
        employeesModel em = mm.map(ed, employeesModel.class);
        em = create(em, ldt);    
        if(ed.getDeptEmp().size()>0){
            for (deptDTO dd : ed.getDeptEmp()) {
                deptEmpModel dem = mm.map(dd, deptEmpModel.class);
                dem.setEmployees(em);
                des.create(dem, ldt);
            }
        }
        if(ed.getDeptManager().size()>0){
            for (deptDTO dd : ed.getDeptManager()) {
                deptManagerModel dmm = mm.map(dd, deptManagerModel.class);
                dmm.setEmployees(em);
                dms.create(dmm, ldt);
            }
        }
        if(ed.getSalaries().size()>0){
            for (salariesDTO sd : ed.getSalaries()) {
                salariesModel sm = mm.map(sd, salariesModel.class);
                sm.setEmployees(em);
                ss.create(sm, ldt);
            }
        }
        if(ed.getTitles().size()>0){
            for (titlesDTO td : ed.getTitles()) {
                titlesModel tm = mm.map(td, titlesModel.class);
                tm.setEmployees(em);
                ts.create(tm, ldt);
            }
        }
        return responseHandlerUtil.generateResponse();        
    }
    public Object updateEmployees(String id, employeesDTO ed){
        employeesModel em = er.findById(id).orElse(null);
            if(em!=null){
            LocalDateTime ldt = LocalDateTime.now();
            ed.setCreatedAt(em.getCreatedAt());
            em = mm.map(ed, employeesModel.class);
            em.setId(id);
            update(em, ldt);
            return responseHandlerUtil.generateResponse();  
        }
        throw new badRequestException(responseCodeEnumType.e03);
    }
    public Object deleteEmployees(String id){
        employeesModel em = er.findById(id).orElse(null);
        delete(em);
        return responseHandlerUtil.generateResponse();  
    }
    public Object getAllEmployees(){
        List<employeesDTO> led = er.findAll().stream().map(value->{
            employeesDTO ed = mm.map(value, employeesDTO.class);
            List<deptDTO> ldde = der.findById(value.getId()).stream().map(value1->mm.map(value1, deptDTO.class)).toList();
            List<deptDTO> lddm = dmr.findById(value.getId()).stream().map(value1->mm.map(value1, deptDTO.class)).toList();
            List<salariesDTO> lsd = sr.findById(value.getId()).stream().map(value1->mm.map(value1, salariesDTO.class)).toList();
            List<titlesDTO> ltd = tr.findById(value.getId()).stream().map(value1->mm.map(value1, titlesDTO.class)).toList();
            ed.setDeptEmp(ldde);
            ed.setDeptManager(lddm);
            ed.setSalaries(lsd);
            ed.setTitles(ltd);
            return ed;
        }).toList();
        return responseHandlerUtil.generateResponse(led);  
    }
    public employeesModel create(employeesModel em, LocalDateTime createdAt){
        try {
            em.setCreatedAt(createdAt);
            er.save(em);
            return em;
        } catch (Exception e) {
            log.error("employeesService.create -> "+e.getMessage());
            throw new internalServerErrorException(responseCodeEnumType.e04);
        }
    }
    public employeesModel update(employeesModel em, LocalDateTime updateAt){
        try {
            em.setUpdatedAt(updateAt);
            er.save(em);
            return em;
        } catch (Exception e) {
            log.error("employeesService.update -> "+e.getMessage());
            throw new internalServerErrorException(responseCodeEnumType.e05);
        }
    }
    public void delete(employeesModel em){
        try {
            List<deptEmpModel> lde = der.findByEmployees(em);
            List<deptManagerModel> ldmm = dmr.findByEmployees(em);
            List<salariesModel> lsm = sr.findByEmployees(em);
            List<titlesModel> ltm = tr.findByEmployees(em);
            der.deleteAll(lde);
            dmr.deleteAll(ldmm);
            sr.deleteAll(lsm);
            tr.deleteAll(ltm);            
            er.delete(em);
        } catch (Exception e) {
            log.error("employeesService.delete -> "+e.getMessage());
            throw new internalServerErrorException(responseCodeEnumType.e06);
        }
    }
}
