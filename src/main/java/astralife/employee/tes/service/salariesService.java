package astralife.employee.tes.service;

import java.time.LocalDateTime;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import astralife.employee.tes.enumType.responseCodeEnumType;
import astralife.employee.tes.exeption.internalServerErrorException;
import astralife.employee.tes.model.salariesModel;
import astralife.employee.tes.repository.salariesRepository;
import jakarta.transaction.Transactional;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
@Transactional
public class salariesService {
    @Autowired
    private salariesRepository sr;
    public salariesModel create(salariesModel sm, LocalDateTime createdAt){
        try {
            sm.setCreatedAt(createdAt);
            sr.save(sm);
            return sm;
        } catch (Exception e) {
            log.error("salariesService.create -> "+e.getMessage());
            throw new internalServerErrorException(responseCodeEnumType.e04);
        }
    }
}
