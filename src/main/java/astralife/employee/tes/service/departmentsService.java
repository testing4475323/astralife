package astralife.employee.tes.service;

import java.time.LocalDateTime;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import astralife.employee.tes.dto.departmentsDTO;
import astralife.employee.tes.enumType.responseCodeEnumType;
import astralife.employee.tes.exeption.badRequestException;
import astralife.employee.tes.exeption.internalServerErrorException;
import astralife.employee.tes.model.departmentsModel;
import astralife.employee.tes.model.deptEmpModel;
import astralife.employee.tes.model.deptManagerModel;
import astralife.employee.tes.repository.departmentsRepository;
import astralife.employee.tes.repository.deptEmpRepository;
import astralife.employee.tes.repository.deptManagerRepository;
import astralife.employee.tes.util.responseHandlerUtil;
import jakarta.transaction.Transactional;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
@Transactional
public class departmentsService {
    @Autowired
    private ModelMapper mm;
    @Autowired
    private departmentsRepository dr;
    @Autowired
    private deptEmpRepository der;
    @Autowired
    private deptManagerRepository dmr;
    public Object createDepartments(departmentsDTO dd){
        LocalDateTime ldt = LocalDateTime.now();
        departmentsModel dm = mm.map(dd, departmentsModel.class);
        create(dm, ldt);    
        return responseHandlerUtil.generateResponse(); 
    }
    public Object updateDepartments(String id, departmentsDTO dd){
        departmentsModel dm = dr.findById(id).orElse(null);
        if(dm!=null){
            LocalDateTime ldt = LocalDateTime.now();
            dd.setCreatedAt(dm.getCreatedAt());
            dm = mm.map(dd, departmentsModel.class);
            dm.setId(id);
            update(dm, ldt);    
            return responseHandlerUtil.generateResponse();
        } 
        throw new badRequestException(responseCodeEnumType.e03);
    }
    public Object deleteDepartments(String id){
        departmentsModel dm = dr.findById(id).orElse(null);
        delete(dm);
        return responseHandlerUtil.generateResponse(); 
    }
    public Object getAllDepartments(){
        List<departmentsDTO> ldd = dr.findAll().stream().map(value-> mm.map(value, departmentsDTO.class)).toList();
        return responseHandlerUtil.generateResponse(ldd);  
    }
    public departmentsModel create(departmentsModel dm, LocalDateTime createdAt){
        try {
            dm.setCreatedAt(createdAt);
            dr.save(dm);
            return dm;
        } catch (Exception e) {
            log.error("departmentsService.create -> "+e.getMessage());
            throw new internalServerErrorException(responseCodeEnumType.e04);
        }
    }
    public departmentsModel update(departmentsModel dm, LocalDateTime updateAt){
        try {
            dm.setUpdatedAt(updateAt);
            dr.save(dm);
            return dm;
        } catch (Exception e) {
            log.error("departmentsService.update -> "+e.getMessage());
            throw new internalServerErrorException(responseCodeEnumType.e05);
        }
    }
    public void delete(departmentsModel dm){
        try {
            List<deptEmpModel> lde = der.findByDepartments(dm);
            List<deptManagerModel> ldmm = dmr.findByDepartments(dm);
            der.deleteAll(lde);
            dmr.deleteAll(ldmm);
            dr.delete(dm);
        } catch (Exception e) {
            log.error("departmentsService.delete -> "+e.getMessage());
            throw new internalServerErrorException(responseCodeEnumType.e06);
        }
    }
}
