package astralife.employee.tes.enumType;

public enum responseCodeEnumType {
    e00("Succeed"),
    e01("Access to this resource on the server is denied"),
    e02("Field cannot valid"),
    e03("Departments not exist"),
    e04("Failed to create"),
    e05("Failed to update"),
    e06("Failed to delete"),
    e07("Already exist"),
    e08("Employees not exist");
    String value;
    private responseCodeEnumType(String value){
        this.value=value;
    }
    public String getValue(){
        return this.value;
    }    
}
